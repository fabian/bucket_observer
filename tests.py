import os
import unittest
from unittest.mock import MagicMock, Mock

from bucket_observer.observers import BucketObserver, Key


class Tests(unittest.TestCase):

    def setUp(self):
        self.db_file_path = os.path.sep.join([
            os.path.abspath(os.path.dirname(__file__)),
            '.test_db.json'
        ])
        self.observer = BucketObserver(
            bucket_name='testbucket',
            access_key_id='FAKED',
            secret_access_key='FAKED',
            db_file_path=self.db_file_path,
            region_name='eu-central-1'
        )
        self.observer._key_iterator = MagicMock()
        self.observer._key_iterator.return_value = []

    def tearDown(self):
        try:
            os.remove(self.db_file_path)
        except FileNotFoundError:
            pass

    def test_create(self):
        key = Key('alice', '', 1, '', 0, '', '')
        self.observer._key_iterator.return_value = [key]

        observed = self.observer.observe()

        self.assertTrue(key in observed['created'])
        self.assertEqual(len(observed['created']), 1)
        self.assertFalse(observed['exists'])
        self.assertFalse(observed['updated'])
        self.assertFalse(observed['deleted'])

    def test_create_signal(self):
        key = Key('alice', '', 1, '', 0, '', '')
        self.observer._key_iterator.return_value = [key]

        def fn():
            pass

        callback = Mock(spec=fn)
        self.observer.key_created.connect(callback)

        self.observer.observe()

        self.assertTrue(callback.called)

    def test_exists(self):
        key = Key('alice', '', 1, '', 0, '', '')
        self.observer._key_iterator.return_value = [key]

        observed = self.observer.observe()

        observed = self.observer.observe()

        self.assertTrue(key in observed['exists'])
        self.assertEqual(len(observed['exists']), 1)
        self.assertFalse(observed['created'])
        self.assertFalse(observed['updated'])
        self.assertFalse(observed['deleted'])

    def test_exists_signal(self):
        key = Key('alice', '', 1, '', 0, '', '')
        self.observer._key_iterator.return_value = [key]

        self.observer.observe()

        def fn():
            pass

        callback = Mock(spec=fn)
        self.observer.key_exists.connect(callback)

        self.observer.observe()

        self.assertTrue(callback.called)

    def test_update(self):
        key = Key('alice', '', 1, '', 0, '', '')
        self.observer._key_iterator.return_value = [key]

        observed = self.observer.observe()

        key = Key('alice', 'UPDATED', 1, '', 0, '', '')
        self.observer._key_iterator.return_value = [key]

        observed = self.observer.observe()

        self.assertTrue(key in observed['updated'])
        self.assertEqual(len(observed['updated']), 1)
        self.assertFalse(observed['created'])
        self.assertFalse(observed['exists'])
        self.assertFalse(observed['deleted'])

    def test_update_signal(self):
        key = Key('alice', '', 1, '', 0, '', '')
        self.observer._key_iterator.return_value = [key]

        self.observer.observe()

        def fn():
            pass

        callback = Mock(spec=fn)
        self.observer.key_updated.connect(callback)

        key = Key('alice', 'UPDATED', 1, '', 0, '', '')
        self.observer._key_iterator.return_value = [key]

        self.observer.observe()

        self.assertTrue(callback.called)

    def test_deleted(self):
        key = Key('alice', '', 1, '', 0, '', '')
        self.observer._key_iterator.return_value = [key]

        observed = self.observer.observe()

        self.observer._key_iterator.return_value = []

        observed = self.observer.observe()

        self.assertTrue(key in observed['deleted'])
        self.assertEqual(len(observed['deleted']), 1)
        self.assertFalse(observed['created'])
        self.assertFalse(observed['exists'])
        self.assertFalse(observed['updated'])

    def test_delete_signal(self):
        key = Key('alice', '', 1, '', 0, '', '')
        self.observer._key_iterator.return_value = [key]

        self.observer.observe()

        def fn():
            pass

        callback = Mock(spec=fn)
        self.observer.key_deleted.connect(callback)

        self.observer._key_iterator.return_value = []

        self.observer.observe()

        self.assertTrue(callback.called)

    def test_bucket_observer_is_callable_and_triggers_observe_method(self):
        self.observer.observe = Mock()
        self.observer()
        self.assertTrue(self.observer.observe.called)

    def test_bucket_observer_and_key_are_avail_on_a_module_level(self):
        from bucket_observer import BucketObserver as BO
        self.assertEqual(BucketObserver, BO)
        from bucket_observer import Key as K
        self.assertEqual(Key, K)


if __name__ == '__main__':
    unittest.main()
