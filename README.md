# Bucket Observer
Pulls S3 buckets, observes changes and emits blinker signals.

```
from bucket_observer import BucketObserver


observe = BucketObserver(
    bucket_name='my-s3-storage',
    access_key_id='AKIAIOSFODNN7EXAMPLE',
    secret_access_key='wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY',
    db_file_path='~/.my-s3-storage-db.json',
    region_name='eu-central-1'
)

@observe.key_created.connect
def on_key_created(sender, key):
    pass

@observe.key_exists.connect
def on_key_exists(sender, key):
    pass

@observe.key_updated.connect
def on_key_updated(sender, key):
    pass

@observe.key_deleted.connect
def on_key_deleted(sender, key):
    pass

# Returns a dictionary with observations
observed = observe()  
```
